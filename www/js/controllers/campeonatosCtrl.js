angular.module('Controllers')

.controller('CampeonatosCtrl', function($scope, $http) {
  
  $scope.listaCampeonatos = {};
  
  syncURL = "https://hml-apifasesgrupos.gazetaesportiva.net/?token=7f0d356ba72a0ba238f2d853e3509d07";
  
  $http({method: 'GET', url: syncURL})
    .success(function(data) {
        $scope.listaCampeonatos = data;
    }).error(function(response) {
        console.log(response);
    });

})