angular.module('starter', ['ionic', 'Controllers']);

angular.module('starter').run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if (window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})
;

angular.module('starter')

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('app.search', {
    url: '/search',
    views: {
      'menuContent': {
        templateUrl: 'templates/search.html'
      }
    }
  })

  .state('app.browse', {
      url: '/browse',
      views: {
        'menuContent': {
          templateUrl: 'templates/browse.html'
        }
      }
    })
    .state('app.playlists', {
      url: '/playlists',
      views: {
        'menuContent': {
          templateUrl: 'templates/playlists.html',
          controller: 'PlaylistsCtrl'
        }
      }
    })

  .state('app.single', {
    url: '/playlists/:playlistId',
    views: {
      'menuContent': {
        templateUrl: 'templates/playlist.html',
        controller: 'PlaylistCtrl'
      }
    }
  })

    .state('app.campeonatos', {
      url: '/campeonatos',
      views: {
        'menuContent': {
          templateUrl: 'templates/campeonatos.html',
          controller: 'CampeonatosCtrl'
        }
      }
    })
  ;
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('app/search');
})
;
//Criando o modulo, aqui pode injetar as dependencias para o modulo
angular.module('Controllers', []);
//utilizando o modulo Controller que foi criado no _module.js
angular.module('Controllers')

.controller('AppCtrl', function($scope, $ionicModal, $timeout, $http) {

  // Form data for the login modal
  $scope.loginData = {};
 
  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };

    
});
angular.module('Controllers')

.controller('CampeonatosCtrl', function($scope, $http) {
  
  $scope.listaCampeonatos = {};
  
  syncURL = "https://hml-apifasesgrupos.gazetaesportiva.net/?token=7f0d356ba72a0ba238f2d853e3509d07";
  
  $http({method: 'GET', url: syncURL})
    .success(function(data) {
        $scope.listaCampeonatos = data;
    }).error(function(response) {
        console.log(response);
    });

})